import os
import numpy as np
from keras.layers import Convolution2D, Flatten, Dense, Input, Dropout, Cropping2D
from keras.models import Sequential
from keras.layers import Lambda
from keras.optimizers import Adam
import tensorflow as tf
import csv
import scipy.misc
import pickle 
import glob

from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle as shuffle
import cv2 as cv2

# Load a pickle file
def load_pickle(file_path):
    with open(file_path, mode='rb') as f:
        file_data = pickle.load(f)
        return file_data;
# Create a pickle    
def create_pickle(file_path,data):
    pickle.dump(data, open(file_path, "wb" ))
    print("Data saved in", file_path)

def load_dataset():
    samples = []
    with open('./data/driving_log.csv') as csvfile:
        reader = csv.reader(csvfile)
        header = True
        for line in reader:
            if not header:
                samples.append(line)
            header = False

    balanced_dataset = balance_dataset(np.array(samples))
    train_samples, validation_samples = train_test_split(balanced_dataset, test_size=0.2)
    return train_samples, validation_samples


def balance_dataset(dataset):
    # data set mostly of zero steer
    # so balance the dataset to remove bias
    non_zero_steer_samples = dataset[np.where(dataset[:, 3] != ' 0')]
    zero_steer_samples = dataset[np.where(dataset[:, 3] == ' 0')]
    print("Original ", len(dataset), "Non zero steering ", len(non_zero_steer_samples), "Zero steering ",
          len(zero_steer_samples))
    max_count_of_non_zero_steer = np.max(np.unique(non_zero_steer_samples[:, 3], return_counts=True)[1])
    selected_zero_steer_samples = zero_steer_samples[
        np.random.choice(np.arange(len(zero_steer_samples)), size=max_count_of_non_zero_steer * 3)]
    
    balanced_dataset = np.append(non_zero_steer_samples, selected_zero_steer_samples, axis=0)
    return shuffle(balanced_dataset)



def flip_image(img):
    return cv2.flip(img, 1)

def pre_process_img(image):
    #return cv2.cvtColor(image,cv2.COLOR_BGR2YUV)
    return image

def generator(samples, batch_size=12):
    num_samples = len(samples)
    while 1:  # Loop forever so the generator never terminates
        shuffle(samples)
        for offset in range(0, num_samples, batch_size):
            batch_samples = samples[offset:offset + batch_size]
            images = []
            angles = []
            for batch_sample in batch_samples:
                name = './data/IMG/' + batch_sample[0].split('/')[-1]
                center_image = cv2.imread(name)
                center_steer = float(batch_sample[3])
                images.append(center_image)
                angles.append(center_steer)

                images.append(flip_image(center_image))
                angles.append(center_steer)

                correction = 0.25  # this is a parameter to tune

                name = './data/IMG/' + batch_sample[1].split('/')[-1]
                left_image = cv2.imread(name)
                left_steer = center_steer + correction  # correction
                images.append(left_image)
                angles.append(left_steer)

                images.append(flip_image(left_image))
                angles.append(left_steer * -1.0)

                name = './data/IMG/' + batch_sample[2].split('/')[-1]
                right_image = cv2.imread(name)
                right_steer = center_steer - correction  # correction
                images.append(right_image)
                angles.append(right_steer)

                images.append(flip_image(right_image))
                angles.append(right_steer * -1.0)

            resized  = []
            for img in images:
                processed_img = pre_process_img(cv2.resize(img[50:140,:,:], (200, 66)))
                resized.append(processed_img)


            y_train = np.array(angles)
            X_train = np.array(resized)
            yield shuffle(X_train, y_train)


def make_model():
    model = Sequential()
    model.add(Lambda(lambda x: (x / 255.0) - 0.5, input_shape=(66, 200, 3)))
    # model.add(Lambda(lambda x: (x / 255.0) - 0.5, input_shape=(106, 200, 3)))

    #  crop  - (top,bottom) (left,right)
    # model.add(Cropping2D(cropping=((40, 0), (0, 0))))
    # model.add(Lambda(lambda x: (x / 255.0) - 0.5))
    # nvidia input size 200w x 66h 
    model.add(Convolution2D(24, 5, 5, subsample=(2, 2), border_mode="valid", activation="relu"))
    model.add(Convolution2D(36, 5, 5, subsample=(2, 2), border_mode="valid", activation="relu"))
    model.add(Convolution2D(48, 5, 5, subsample=(2, 2), border_mode="valid", activation="relu"))
    model.add(Convolution2D(64, 3, 3, subsample=(1, 1), border_mode="valid", activation="relu"))
    model.add(Dropout(.8))
    model.add(Convolution2D(64, 3, 3, subsample=(1, 1), border_mode="valid", activation="relu"))
    model.add(Dropout(.8))

    # nvidia architecture layer has 1164 params at this level. but how ???
    model.add(Flatten())
    model.add(Dropout(.7))
    model.add(Dense(100))
    model.add(Dropout(.7))
    model.add(Dense(50))
    #model.add(Dropout(.7))
    model.add(Dense(10))
    model.add(Dense(1))
    adam = Adam(lr=0.00001)
    model.compile(optimizer=adam, loss="mse")
    return model


def main():
    train_samples, validation_samples = load_dataset()
    batch_size = 128
    train_generator = generator(train_samples, batch_size=batch_size)
    validation_generator = generator(validation_samples, batch_size=batch_size)

    # ch, row, col = 3, 80, 320  # Trimmed image format

    model = make_model()
    # print(model.to_json())
    model.summary()
    
    history_object = model.fit_generator(train_generator,
                        samples_per_epoch=len(train_samples) * 6 ,
                        validation_data=validation_generator,
                        nb_val_samples=len(validation_samples) * 6 , nb_epoch=10)

    if not os.path.exists('./model/steering/'):
        os.makedirs('./model/steering/')

    model_no = len(glob.glob('./model/steering/*.h5')) + 1
    model.save(str('./model/steering/model_' + str(model_no) + '.h5'))
    create_pickle(str('./model/steering/model_' + str(model_no) + '_history.p'),history_object.history)
    print("Model Saved ")


if __name__ == '__main__':
    main()
