import os
import numpy as np
import keras
from keras.layers import Convolution2D, Flatten, Dense, Input, Dropout, Cropping2D, ELU
from keras.models import Sequential
from keras.layers import Lambda
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, Callback
from keras.regularizers import l2, activity_l2


import tensorflow as tf
import csv
import scipy.misc
import pickle
import glob
from pandas import read_csv
import pandas as pd


from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle as shuffle
import cv2 as cv2


# Load a pickle file
def load_pickle(file_path):
    with open(file_path, mode='rb') as f:
        file_data = pickle.load(f)
        return file_data;


# Create a pickle
def create_pickle(file_path, data):
    pickle.dump(data, open(file_path, "wb"))
    print("Data saved in", file_path)

def augment_brightness(image):
    image1 = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    random_bright = .25 + np.random.uniform()
    image1[:, :, 2] = image1[:, :, 2] * random_bright
    image1 = cv2.cvtColor(image1, cv2.COLOR_HSV2BGR)
    return image1


def random_translate(image, steeringAngle):
    rows, cols, _ = image.shape
    transRange = 100
    numPixels = 10
    valPixels = 0.4
    transX = transRange * np.random.uniform() - transRange / 2
    steeringAngle = steeringAngle + transX / transRange * 2 * valPixels
    transY = numPixels * np.random.uniform() - numPixels / 2
    transMat = np.float32([[1, 0, transX], [0, 1, transY]])
    image = cv2.warpAffine(image, transMat, (cols, rows))
    return image, steeringAngle


def flip_image(img):
    return cv2.flip(img, 1)


def preprocess_image(image):

    # crop & scale
    img = cv2.resize(image[50:140, :, :], (200, 66))

    # blur
    #img = cv2.GaussianBlur(img, (3, 3), 0)
    #img = augment_brightness(img)
    # format
    img = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
    return img

def load_dataset():
    paths = []
    paths.append('./data/driving_log.csv')
    paths.append('./data/sharp_turn.csv')
    paths.append('./data/curves_driving_log.csv')
    paths.append('./data/long_drive.csv')

    
    samples = []
    img_path = []
    angles = []
    correction = 0.2  # this is a parameter to tune
    x = np.array([])
    y = np.array([])
    for path in paths:
        img_path = []
        angles = []
        with open(path) as csvfile:
            reader = csv.reader(csvfile)
            header = True
            for line in reader:
                if not header:
                    
                    if float(line[6]) < 0.1 :
                        continue

                    name = './data/' + line[0].strip()
                    center_steer = float(line[3])
                    img_path.append(name)
                    angles.append(center_steer)

                    if center_steer != 9999:
                        name = './data/' + line[1].strip()
                        left_steer = center_steer + correction  # correction
                        if np.abs(left_steer) != 99999 and np.abs(left_steer) != 9999:
                            img_path.append(name)
                            angles.append(left_steer)

                        name = './data/' + line[2].strip()
                        right_steer = center_steer - correction  # correction
                        if np.abs(right_steer)  != 9999 and np.abs(left_steer) != 9999:
                            img_path.append(name)
                            angles.append(right_steer)

                header = False
                
#             if udacity:
#                 x, y = balance_dataset(np.asarray(img_path), np.asarray(angles))
#                 udacity = False
#                 print("loaded udacity")
#             else:
                
            x = np.append(x,np.array(img_path))
            y = np.append(y,np.array(angles))


                
    return shuffle(x, y)

def balance_dataset(path, angles):
    # data set mostly of zero steer
    # so balance the dataset to remove bia
    num_bins = 41
    avg_samples_per_bin = int(len(angles)/num_bins)
#     avg_samples_per_bin = 800
    path = np.array(path)
    ang  = np.array(angles)
    path,ang = shuffle(path,ang)
    balanced_path  = np.array([])
    balanced_angles = np.array([])
    bin_start = 0
    max_items_per_bin = avg_samples_per_bin
    count = 0
    bin_start = .3
    print(num_bins,avg_samples_per_bin)
    for bin_end in np.linspace(.3,1.001,num=num_bins):
        index = np.where((np.abs(ang[:]) >= bin_start) & (np.abs(ang[:]) < bin_end))[0]
        count = count + len(index) 
    #     print(bin_start,bin_end,len(index))
        selected_ang = ang[index]
        selected_path = path[index]

        selected_ang,selected_path = shuffle(selected_ang,selected_path)
        item_count  = min(max_items_per_bin,len(index))
        #print(bin_start,bin_end,len(index), item_count)
        selected_ang = selected_ang[0:item_count]
        selected_path = selected_path[0:item_count]
#         print(bin_start,bin_end,len(index), item_count,len(selected_ang))
        
        balanced_angles = np.append(balanced_angles,selected_ang)
        balanced_path = np.append(balanced_path,selected_path)
        bin_start = bin_end
    
    return shuffle(balanced_path, balanced_angles) 

def mirror_dataset(x,y):
    print(len(x))
    aug_x = []
    aug_y = []
    for i in range(len(x)):
#         img = cv2.imread(x[i])
        aug_x.append("flip_" + x[i])
        aug_y.append(y[i] * -1.)
    print(len(aug_x),len(aug_y))
    return shuffle(np.append(x,np.asarray(aug_x)),np.append(y,np.asarray(aug_y)))


def make_model():
    model = Sequential()
    #model.add(Lambda(lambda x: (x / 255.0) - 0.5, input_shape=(66, 200, 3)))
    model.add(Lambda(lambda x: (x / 127.5) - 1., input_shape=(66, 200, 3)))
    # model.add(Lambda(lambda x: (x / 255.0) - 0.5, input_shape=(106, 200, 3)))

    #  crop  - (top,bottom) (left,right)
    # model.add(Cropping2D(cropping=((40, 0), (0, 0))))
    # model.add(Lambda(lambda x: (x / 255.0) - 0.5))
    # nvidia input size 200w x 66h
    #W_regularizer=l2(0.001)
    model.add(Convolution2D(24, 5, 5, subsample=(2, 2), border_mode="valid", activation="relu"))
    model.add(Convolution2D(36, 5, 5, subsample=(2, 2), border_mode="valid", activation="relu"))
    model.add(Convolution2D(48, 5, 5, subsample=(2, 2), border_mode="valid", activation="relu"))
    model.add(Dropout(.25))
    model.add(Convolution2D(64, 3, 3, border_mode="valid", activation="relu"))
    model.add(Dropout(.25))
    model.add(Convolution2D(64, 3, 3, border_mode="valid", activation="relu"))
    model.add(Dropout(.25))

    # nvidia architecture layer has 1164 params at this level. but how ???
    model.add(Flatten())
    model.add(Dense(100,activation='relu'))
    model.add(Dropout(.25))
    model.add(Dense(50,activation='relu'))
   
    model.add(Dense(10,activation='relu'))
    model.add(Dense(1))
    adam = Adam(lr=0.0001)
    model.compile(optimizer=adam, loss="mse")
    return model

def make_model_copy():
    model = Sequential()

    # Normalize
    model.add(Lambda(lambda x: x/127.5 - 1.0,input_shape=(66,200,3)))

    # Add three 5x5 convolution layers (output depth 24, 36, and 48), each with 2x2 stride
    model.add(Convolution2D(24, 5, 5, subsample=(2, 2), border_mode='valid', W_regularizer=l2(0.001)))
    model.add(ELU())
    model.add(Convolution2D(36, 5, 5, subsample=(2, 2), border_mode='valid', W_regularizer=l2(0.001)))
    model.add(ELU())
    model.add(Convolution2D(48, 5, 5, subsample=(2, 2), border_mode='valid', W_regularizer=l2(0.001)))
    model.add(ELU())

    #model.add(Dropout(0.50))
    
    # Add two 3x3 convolution layers (output depth 64, and 64)
    model.add(Convolution2D(64, 3, 3, border_mode='valid', W_regularizer=l2(0.001)))
    model.add(ELU())
    model.add(Convolution2D(64, 3, 3, border_mode='valid', W_regularizer=l2(0.001)))
    model.add(ELU())

    # Add a flatten layer
    model.add(Flatten())

    # Add three fully connected layers (depth 100, 50, 10), tanh activation (and dropouts)
    model.add(Dense(100, W_regularizer=l2(0.001)))
    model.add(ELU())
    #model.add(Dropout(0.50))
    model.add(Dense(50, W_regularizer=l2(0.001)))
    model.add(ELU())
    #model.add(Dropout(0.50))
    model.add(Dense(10, W_regularizer=l2(0.001)))
    model.add(ELU())
    #model.add(Dropout(0.50))

    # Add a fully connected output layer
    model.add(Dense(1))

    # Compile and train the model, 
    #model.compile('adam', 'mean_squared_error')
    model.compile(optimizer=Adam(lr=1e-3), loss='mse')
    return model

def balance(image_paths,angles):
    num_bins = 23
    avg_samples_per_bin = len(angles)/num_bins
    hist, bins = np.histogram(angles, num_bins)
    keep_probs = []
    target = avg_samples_per_bin * .5
    for i in range(num_bins):
        if hist[i] < target:
            keep_probs.append(1.)
        else:
            keep_probs.append(1./(hist[i]/target))
    remove_list = []
    for i in range(len(angles)):
        for j in range(num_bins):
            if angles[i] > bins[j] and angles[i] <= bins[j+1]:
                # delete from X and y with probability 1 - keep_probs[j]
                if np.random.rand() > keep_probs[j]:
                    remove_list.append(i)
    image_paths = np.delete(image_paths, remove_list, axis=0)
    angles = np.delete(angles, remove_list)
    
    return image_paths,angles
    
def dataset_generator(img_path, angles, batch_size=128,augment=True):
    #print(np.array(img_path).shape,np.array(angles).shape)
   
    X = []
    y = []
    while True:
        path, angles = shuffle(img_path, angles)
        for i in range(len(angles)):

            path = img_path[i];
            img = None
            if path.startswith("flip_"):
                path = path.replace("flip_","")
                img = cv2.imread(path)
                img = flip_image(img)
            else:
                img = cv2.imread(path)

                
            angle = angles[i]
            processed_img = preprocess_image(img)
           
            
            bias = 0.8
            steer_magnitude_thresh = np.random.rand()
            
            #if not augment:
            X.append(processed_img)
            y.append(angle)
            #else:
             #   if (abs(angle) + bias) < steer_magnitude_thresh:
            
             # else:
                #    X.append(processed_img)
                #    y.append(angle)
        
            if len(X) == batch_size:
                #print("\n")
                #print(np.histogram(np.array(y))[0])
                yield (np.array(X), np.array(y))
                #print("New batch !!! ", i)
                X, y = [], []
                
            if np.random.randint(0,100) > 50 and augment:        
                processed_image, new_angle = random_translate(img, angle)
                processed_img = preprocess_image(processed_image)
                X.append(processed_img)
                y.append(new_angle)

                if len(X) == batch_size:
                    yield (np.array(X), np.array(y))
                    X, y = [], []



def main():
    
    x,y = load_dataset()
    x,y = balance(x,y)
    x,y = mirror_dataset(x,y)
    

    X_train, X_valid, y_train, y_valid = train_test_split(x, y, test_size=0.2)
    
    batch_size = 128
 
    #X_train = X_train[0:2000]
    #y_train = y_train[0:2000]
    #X_valid = X_valid[0:500]
    #y_valid = y_valid[0:500]
    
    
    train_generator = dataset_generator(X_train, y_train, batch_size=batch_size, augment=True)
    validation_generator = dataset_generator(X_valid, y_valid, batch_size=batch_size,augment=False)

    # ch, row, col = 3, 80, 320  # Trimmed image format
    print("Train ", len(X_train), "Valid ",len(X_valid))
    samples_per_epoch = int(len(X_train) / batch_size) * batch_size
    nb_val_samples = int(len(X_valid) / batch_size) * batch_size
    #samples_per_epoch = 20480 
    #nb_val_samples = int(samples_per_epoch * .20)
    
    #samples_per_epoch = batch_size * 100
    #nb_val_samples = batch_size * 10
    
    print(len(X_train), samples_per_epoch)
    #return
    
    no_of_epocs = 10
    model = make_model_copy()
    # print(model.to_json())
    #model.summary()
    
    if not os.path.exists('./model/steering4/'):
        os.makedirs('./model/steering4/')

    model_no = len(glob.glob('./model/steering4/*.p')) + 1    
        
    checkpoint = ModelCheckpoint('./model/steering4/model_' + str(model_no) +'{epoch:02d}.h5')
    # keras does not write histograms when using generators :(
    tensorboard =  keras.callbacks.TensorBoard(log_dir='./model/steering4', histogram_freq=1, write_graph=True, write_images=False)

    history_object = model.fit_generator(train_generator,
                                         samples_per_epoch = samples_per_epoch,
                                         validation_data=validation_generator,
                                         nb_val_samples=nb_val_samples, 
                                         nb_epoch=no_of_epocs, 
                                         callbacks=[checkpoint],
                                         verbose=1)


    
    model.save(str('./model/steering4/model_' + str(model_no) + '.h5'))
    create_pickle(str('./model/steering4/model_' + str(model_no) + '_history.p'), history_object.history)
    print("Model Saved ")


if __name__ == '__main__':
    main()
