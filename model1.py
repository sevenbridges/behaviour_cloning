import os
import numpy as np
from keras.layers import Convolution2D, Flatten, Dense, Input, Dropout, Cropping2D
from keras.models import Sequential
from keras.layers import Lambda
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, Callback
from keras.regularizers import l2, activity_l2


import tensorflow as tf
import csv
import scipy.misc
import pickle
import glob
from pandas import read_csv
import pandas as pd


from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle as shuffle
import cv2 as cv2


# Load a pickle file
def load_pickle(file_path):
    with open(file_path, mode='rb') as f:
        file_data = pickle.load(f)
        return file_data;


# Create a pickle
def create_pickle(file_path, data):
    pickle.dump(data, open(file_path, "wb"))
    print("Data saved in", file_path)


def load_dataset():
    paths = ['./data/driving_log.csv']
    paths.append('./data/sharp_turn.csv')
    # paths = ['./data/driving_log_balanced.csv']
    
    samples = []
    img_path = []
    angles = []
    correction = 0.25  # this is a parameter to tune
    x = np.array([])
    y = np.array([])
    udacity = True;
    for path in paths:
        img_path = []
        angles = []
        with open(path) as csvfile:
            reader = csv.reader(csvfile)
            header = True
            for line in reader:
                if not header:
                    name = './data/' + line[0].strip()
                    center_steer = float(line[3])
                    img_path.append(name)
                    angles.append(center_steer)

                    
                    name = './data/' + line[1].strip()
                    left_steer = center_steer + correction  # correction
                    img_path.append(name)
                    angles.append(left_steer)

                    name = './data/' + line[2].strip()
                    right_steer = center_steer - correction  # correction
                    img_path.append(name)
                    angles.append(right_steer)

                header = False
                
            if udacity:
                x, y = balance_dataset(np.asarray(img_path), np.asarray(angles))
                udacity = False
                print("loaded udacity")
            else:
                
                x = np.append(x,np.array(img_path))
                y = np.append(y,np.array(angles))


                print(x,y)
                
    return shuffle(x, y)


def balance_dataset(path, angles):
    # data set mostly of zero steer
    # so balance the dataset to remove bias
    
    path = np.array(path)
    ang  = np.array(angles)
    balanced_path  = np.array([])
    balanced_angles = np.array([])
    bin_start = -1
    max_items_per_bin = 200
    count = 0
    for bin_end in np.linspace(-1,1,num=50):
        index = np.where((ang[:] >= bin_start) & (ang[:] < bin_end))[0]
        count = count + len(index) 
    #     print(bin_start,bin_end,len(index))

        selected_ang = ang[index]
        selected_path = path[index]

        selected_ang,selected_path = shuffle(selected_ang,selected_path)
        item_count  = min(max_items_per_bin,len(index))
    #     print(bin_start,bin_end,len(index), item_count)
        selected_ang = selected_ang[0:item_count]
        selected_path = selected_path[0:item_count]
    #     print(bin_start,bin_end,len(index), item_count,len(selected_ang))
        balanced_angles = np.append(balanced_angles,selected_ang)
        balanced_path = np.append(balanced_path,selected_path)
        bin_start = bin_end

    return shuffle(balanced_path, balanced_angles) 

    non_zero_steer_path = path[np.where(angles != 0)]
    non_zero_steer_angles = angles[np.where(angles != 0)]
    zero_steer_path = path[np.where(angles == 0)]
    zero_steer_angles = angles[np.where(angles == 0)]

    print("Original ", len(path), "Non zero steering ", len(non_zero_steer_angles), "Zero steering ",
          len(zero_steer_angles))
    max_count_of_non_zero_steer = np.max(np.unique(non_zero_steer_angles, return_counts=True)[1])
    selected_zero_steer_idx = np.random.choice(np.arange(len(zero_steer_angles)), size=max_count_of_non_zero_steer)
    selected_zero_steer_path = zero_steer_path[selected_zero_steer_idx]
    selected_zero_steer_angles = zero_steer_angles[selected_zero_steer_idx]

    x = np.squeeze(np.append(non_zero_steer_path, selected_zero_steer_path))
    y = np.squeeze(np.append(non_zero_steer_angles, selected_zero_steer_angles))
    return shuffle(x, y)


def augment_brightness(image):
    image1 = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    random_bright = .25 + np.random.uniform()
    image1[:, :, 2] = image1[:, :, 2] * random_bright
    image1 = cv2.cvtColor(image1, cv2.COLOR_HSV2BGR)
    return image1


def random_translate(image, steeringAngle):
    rows, cols, _ = image.shape
    transRange = 100
    numPixels = 10
    valPixels = 0.4
    transX = transRange * np.random.uniform() - transRange / 2
    steeringAngle = steeringAngle + transX / transRange * 2 * valPixels
    transY = numPixels * np.random.uniform() - numPixels / 2
    transMat = np.float32([[1, 0, transX], [0, 1, transY]])
    image = cv2.warpAffine(image, transMat, (cols, rows))
    return image, steeringAngle


def flip_image(img):
    return cv2.flip(img, 1)


def preprocess_image(image):
    # crop & scale
    img = cv2.resize(image[50:140, :, :], (200, 66))
    # blur
    img = cv2.GaussianBlur(img, (3, 3), 0)
    img = augment_brightness(img)
    # format
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img



def dataset_generator(img_path, angles, batch_size=128,augment=True):
    #print(np.array(img_path).shape,np.array(angles).shape)
    print("aug ment", augment)

    X = []
    y = []
    while True:
        path, angles = shuffle(img_path, angles)
        for i in range(len(angles)):
           
            img = cv2.imread(path[i])
            angle = angles[i]
            processed_img = preprocess_image(img)
            X.append(processed_img)
            y.append(angle)

            if len(X) == batch_size:
                yield (np.array(X), np.array(y))
                #path, angles = shuffle(img_path, angles)
                X, y = [], []

                # flip img
            if np.random.randint(0,100) > 50 and augment:
                processed_img = cv2.flip(img, 1)
                processed_img = preprocess_image(processed_img)
                X.append(processed_img)
                y.append(angle * -1)
                if len(X) == batch_size:
                    yield (np.array(X), np.array(y))
                    #path, angles = shuffle(img_path, angles)
                    X, y = [], []

            if np.random.randint(0,100) > 50 and augment:        
                processed_image, new_angle = random_translate(img, angle)
                processed_img = preprocess_image(processed_image)
                X.append(processed_img)
                y.append(new_angle)

                if len(X) == batch_size:
                    yield (np.array(X), np.array(y))
                    #path, angles = shuffle(img_path, angles)
                    X, y = [], []


def make_model():
    model = Sequential()
    model.add(Lambda(lambda x: (x / 255.0) - 0.5, input_shape=(66, 200, 3)))
    # model.add(Lambda(lambda x: (x / 255.0) - 0.5, input_shape=(106, 200, 3)))

    #  crop  - (top,bottom) (left,right)
    # model.add(Cropping2D(cropping=((40, 0), (0, 0))))
    # model.add(Lambda(lambda x: (x / 255.0) - 0.5))
    # nvidia input size 200w x 66h
    #W_regularizer=l2(0.001)
    model.add(Convolution2D(24, 5, 5, subsample=(2, 2), border_mode="valid", activation="relu"))
    model.add(Convolution2D(36, 5, 5, subsample=(2, 2), border_mode="valid", activation="relu"))
    model.add(Convolution2D(48, 5, 5, subsample=(2, 2), border_mode="valid", activation="relu"))
    model.add(Dropout(.7))
    model.add(Convolution2D(64, 3, 3, subsample=(1, 1), border_mode="valid", activation="relu"))
    model.add(Dropout(.7))
    model.add(Convolution2D(64, 3, 3, subsample=(1, 1), border_mode="valid", activation="relu"))
    model.add(Dropout(.7))

    # nvidia architecture layer has 1164 params at this level. but how ???
    model.add(Flatten())
    model.add(Dropout(.7))
    model.add(Dense(100))
    model.add(Dropout(.7))
    model.add(Dense(50))
   
    model.add(Dense(10))
    model.add(Dense(1))
    adam = Adam(lr=0.0001)
    model.compile(optimizer=adam, loss="mse")
    return model

def balance_udacity():
    df = read_csv('data/driving_log.csv')
    balanced = pd.DataFrame()   # Balanced dataset
    bins = 1000                 # N of bins
    bin_n = 200                 # N of examples to include in each bin (at most)

    start = 0
    for end in np.linspace(0, 1, num=bins):  
        df_range = df[(np.absolute(df.steering) >= start) & (np.absolute(df.steering) < end)]
        range_n = min(bin_n, df_range.shape[0])
        if(range_n > 0):
            balanced = pd.concat([balanced, df_range.sample(range_n)])
        start = end
    balanced.to_csv('data/driving_log_balanced.csv', index=False)

def main():
    
    x, y = load_dataset()
    X_train, X_valid, y_train, y_valid = train_test_split(x, y, test_size=0.3)

    batch_size = 128
   
    train_generator = dataset_generator(X_train, y_train, batch_size=batch_size, augment=True)
    validation_generator = dataset_generator(X_valid, y_valid, batch_size=batch_size,augment=False)

    # ch, row, col = 3, 80, 320  # Trimmed image format

    samples_per_epoch = int(len(X_train) / batch_size) * batch_size
    samples_per_epoch = 20480
    print(len(X_train), samples_per_epoch)
    #return
    
    no_of_epocs = 20
    model = make_model()
    # print(model.to_json())
    model.summary()
    
    if not os.path.exists('./model/steering1/'):
        os.makedirs('./model/steering1/')

    model_no = len(glob.glob('./model/steering1/*.p')) + 1    
        
    checkpoint = ModelCheckpoint('./model/steering1/model_' + str(model_no) +'{epoch:02d}.h5')

    history_object = model.fit_generator(train_generator,
                                         samples_per_epoch = samples_per_epoch,
                                         validation_data=validation_generator,
                                         nb_val_samples=len(y_valid), nb_epoch=no_of_epocs, callbacks=[checkpoint])


    
    model.save(str('./model/steering1/model_' + str(model_no) + '.h5'))
    create_pickle(str('./model/steering1/model_' + str(model_no) + '_history.p'), history_object.history)
    print("Model Saved ")


if __name__ == '__main__':
    main()
